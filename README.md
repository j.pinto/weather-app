# Aplicación meteorológica

**Nivel:** 1-Principiante

Una aplicación meteorológica para obtener la temperatura, las condiciones meteorológicas y si es de día o de noche de una ciudad en particular utilizando 'accuweather'. Una API meteorológica gratuita.



## Historias de usuario

- [ ] Introduzca el nombre de una ciudad en el campo 'entrada'.
- [ ] Al presionar enter, el usuario envía el nombre de la ciudad que actualiza el 'DOM' con la temperatura, la condición climática, la imagen del día o la noche y el icono de la condición climática.

## Características adicionales

- [ ] Al cerrar la ventana del navegador, el nombre de la ciudad se almacenará en localStorage y cuando el usuario regrese, el nombre se recuperará para realizar una llamada a la API para actualizar el 'DOM'.

## Useful links and resources

- [localStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage)
- [accuweather](https://developer.accuweather.com/)
- [axios](https://github.com/axios/axios)
- [bootstrap](https://getbootstrap.com/)
